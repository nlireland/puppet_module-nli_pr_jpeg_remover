class pr_jpeg_remover (
  $restricted_url  = $pr_jpeg_remover::params::restricted_url,
  $jpegs_base_path = $pr_jpeg_remover::params::jpegs_base_path,
  $restricted_ids  = $pr_jpeg_remover::params::restricted_ids,
  $install_path    = $pr_jpeg_remover::params::restricted_ids,
  $user            = $pr_jpeg_remover::params::user,
  $group           = $pr_jpeg_remover::params::group,
) inherits pr_jpeg_remover::params {

  file {"$install_path":
    ensure  => directory,
    owner   => $user,
  }

  file { 'remove_jpegs_script':
    ensure  => present,
    owner   => $user,
    group   => $group,
    mode    => '0755',
    path    => "$install_path/remove_jpegs.rb",
    content => template('pr_jpeg_remover/remove_jpegs.rb.erb'),
    require => File["$install_path"]
  }
}
