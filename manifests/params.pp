class pr_jpeg_remover::params {
  $restricted_url  = 'http://localhost'
  $jpegs_base_path = '/tmp/path/to/jpegs'
  $restricted_ids  = "[]"
  $install_path    = '/tmp/install/'
  $user            = 'user'
  $group           = 'group'
}
