# README #

A puppet module create a simple ruby script for removing unwanted static jpegs for the pr site

## Usage ##


```
#!puppet

node 'mynode' {
  class {'nli_pr_jpeg_remover':
    restricted_url  => 'http://localhost',
    jpegs_base_path => '/tmp/path/to/jpegs',
    restricted_ids  => ["vtls00000001", "vtls00000002"],
    install_path    => '/tmp/install/',
    user            => 'username',
    group           => 'groupname',
  }
}
```
